$(document).ready(function() {

  // ready for Javascript which will run when the html is loaded
const  faces = $('faces');
const makeFace = function(name) {
return $('<img>' , {
title:name,
src: `img/2018/${name}.jpg`,
class:"face"
});
};
const names =[
              "Thamizh",
              "Akshat",
              "Sanjana",
              "Akhil",
              "Mahidher",
              "Keerthana",
              "RVishnupriya",
              "Anushree",
              "Shruti",
              "Sudeep" ];
names.forEach(function(name){
faces.append(makeFace(name));
});
});
